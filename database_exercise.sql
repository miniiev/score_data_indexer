CREATE TABLE `branch`
(
    id      int,
    country varchar(2),
    state   varchar(2)
);
CREATE TABLE `loan`
(
    id        text,
    branch_id int,
    value     float,
    is_active int
);

insert into branch
values (1, 'US', 'CA'),
       (2, 'US', 'FL');
insert into loan
values (1, 1, 3, 1),
       (2, 1, 1, 1),
       (3, 1, 10, 0),
       (4, 2, 1, 1);




SELECT b.country AS 'Country', b.state AS 'State', COALESCE(AVG(l.value), 0) AS 'Average value of active loans'
FROM branch b
    LEFT JOIN loan l ON l.branch_id = b.id AND l.is_active = 1
GROUP BY b.country, b.state;