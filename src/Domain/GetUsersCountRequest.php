<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\User\Gender;
use App\Domain\User\Region;
use App\Domain\User\Score\Range;

class GetUsersCountRequest
{
    private ?Range $scoreRange = null;

    private ?Region $region = null;

    private ?Gender $gender = null;

    private ?bool $hasLegalAge = null;

    private ?bool $hasPositiveScore = null;

    public static function createScoreRange(Range $scoreRange): self
    {
        $request = new self();
        $request->scoreRange = $scoreRange;

        return $request;
    }

    public static function createCondition(Region $region, Gender $gender, bool $hasLegalAge, bool $hasPositiveScore): self
    {
        $request = new self();
        $request->region = $region;
        $request->gender = $gender;
        $request->hasLegalAge = $hasLegalAge;
        $request->hasPositiveScore = $hasPositiveScore;

        return $request;
    }

    public function hasScoreRange(): bool
    {
        return $this->scoreRange !== null;
    }

    public function getScoreRange(): Range
    {
        return $this->scoreRange;
    }

    public function hasRegion(): bool
    {
        return $this->region !== null;
    }

    public function getRegion(): Region
    {
        return $this->region;
    }

    public function hasGender(): bool
    {
        return $this->gender !== null;
    }

    public function getGender(): Gender
    {
        return $this->gender;
    }

    public function hasHasLegalAge(): bool
    {
        return $this->hasLegalAge !== null;
    }

    public function getHasLegalAge(): bool
    {
        return $this->hasLegalAge;
    }

    public function hasHasPositiveScore(): bool
    {
        return $this->hasPositiveScore !== null;
    }

    public function getHasPositiveScore(): bool
    {
        return $this->hasPositiveScore;
    }
}