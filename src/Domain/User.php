<?php

declare(strict_types=1);

namespace App\Domain;

use App\Domain\User\Age;
use App\Domain\User\Gender;
use App\Domain\User\Name;
use App\Domain\User\Region;
use App\Domain\User\Score;

class User
{
    private Name $name;

    private Gender $gender;

    private Age $age;

    private Region $region;

    private Score $score;

    public function __construct(Name $name, Gender $gender, Age $age, Region $region, Score $score)
    {
        $this->name = $name;
        $this->gender = $gender;
        $this->age = $age;
        $this->region = $region;
        $this->score = $score;
    }

    public function getName(): Name
    {
        return $this->name;
    }

    public function getGender(): Gender
    {
        return $this->gender;
    }

    public function getAge(): Age
    {
        return $this->age;
    }

    public function getRegion(): Region
    {
        return $this->region;
    }

    public function getScore(): Score
    {
        return $this->score;
    }
}