<?php

declare(strict_types=1);

namespace App\Domain\User;

use InvalidArgumentException;

class Score
{
    private const MIN_ALLOWED_SCORE = -100;

    private const MAX_ALLOWED_SCORE = 100;

    private int $value;

    public function __construct(int $value)
    {
        if ($value < self::MIN_ALLOWED_SCORE || $value > self::MAX_ALLOWED_SCORE) {
            throw new InvalidArgumentException(
                sprintf(
                    'SCORE must be in interval [%d,%d]. Got: %d',
                    self::MIN_ALLOWED_SCORE,
                    self::MAX_ALLOWED_SCORE,
                    $value
                )
            );
        }

        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function isPositive(): bool
    {
        return $this->value > 0;
    }

    public function equals(Score $score): bool
    {
        return $this->value === $score->value;
    }

    public function greaterThan(Score $score): bool
    {
        return $this->value > $score->value;
    }

    public function lessThan(Score $score): bool
    {
        return $this->value < $score->value;
    }

    public function __toString(): string
    {
        return (string)$this->getValue();
    }
}