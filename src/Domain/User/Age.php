<?php

declare(strict_types=1);

namespace App\Domain\User;

use InvalidArgumentException;

class Age
{
    private const MIN_ALLOWED_AGE = 0;

    private const MAX_ALLOWED_AGE = 100;

    private const MIN_LEGAL_AGE = 21;

    private int $value;

    public function __construct(int $value)
    {
        if ($value < self::MIN_ALLOWED_AGE || $value > self::MAX_ALLOWED_AGE) {
            throw new InvalidArgumentException(
                sprintf(
                    'Age must be in interval [%d,%d]. Got: %d',
                    self::MIN_ALLOWED_AGE,
                    self::MAX_ALLOWED_AGE,
                    $value
                )
            );
        }

        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function isLegal(): bool
    {
        return $this->getValue() >= self::MIN_LEGAL_AGE;
    }

    public function equals(Age $age): bool
    {
        return $this->value === $age->value;
    }
}