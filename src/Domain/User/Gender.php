<?php

declare(strict_types=1);

namespace App\Domain\User;

class Gender
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Gender $gender): bool
    {
        return $this->value === $gender->value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}