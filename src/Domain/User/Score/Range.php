<?php

declare(strict_types=1);

namespace App\Domain\User\Score;

use App\Domain\User\Score;
use InvalidArgumentException;

class Range
{
    private Score $start;

    private Score $end;

    public function __construct(Score $start, Score $end)
    {
        if ($end->lessThan($start)) {
            throw new InvalidArgumentException(
                sprintf(
                    'Range end must be greater than range start. Got: [%d,%d]',
                    $start->getValue(),
                    $end->getValue()
                )
            );
        }

        $this->start = $start;
        $this->end = $end;
    }

    public function getStart(): Score
    {
        return $this->start;
    }

    public function getEnd(): Score
    {
        return $this->end;
    }
}