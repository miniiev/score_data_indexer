<?php

declare(strict_types=1);

namespace App\Domain\User;

class Region
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Region $region): bool
    {
        return $this->value === $region->value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}