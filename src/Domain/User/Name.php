<?php

declare(strict_types=1);

namespace App\Domain\User;

class Name
{
    private string $value;

    public function __construct(string $value)
    {
        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function equals(Name $name): bool
    {
        return $this->value === $name->value;
    }

    public function __toString(): string
    {
        return $this->getValue();
    }
}