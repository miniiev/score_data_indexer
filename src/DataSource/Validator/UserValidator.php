<?php

declare(strict_types=1);

namespace App\DataSource\Validator;

use InvalidArgumentException;

class UserValidator
{
    public function validate(array $data): void
    {
        if (!isset($data['Name'])) {
            throw new InvalidArgumentException('Name is missing.');
        }

        if (!isset($data['Gender'])) {
            throw new InvalidArgumentException('Gender is missing.');
        }

        if (!isset($data['Age'])) {
            throw new InvalidArgumentException('Age is missing.');
        }

        if (filter_var($data['Age'], FILTER_VALIDATE_INT) === false) {
            throw new InvalidArgumentException('Age is not integer.');
        }

        if (!isset($data['Region'])) {
            throw new InvalidArgumentException('Region is missing.');
        }

        if (!isset($data['Score'])) {
            throw new InvalidArgumentException('Score is missing.');
        }

        if (filter_var($data['Score'], FILTER_VALIDATE_INT) === false) {
            throw new InvalidArgumentException('Score is not integer.');
        }
    }
}