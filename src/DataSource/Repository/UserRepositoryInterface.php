<?php

declare(strict_types=1);

namespace App\DataSource\Repository;

use App\Domain\GetUsersCountRequest;

interface UserRepositoryInterface
{
    public function getUsersCount(GetUsersCountRequest $request): int;
}