<?php

declare(strict_types=1);

namespace App\DataSource\Repository;

use App\DataSource\Mapper\UserMapper;
use App\DataSource\Reader\ReaderInterface;
use App\DataSource\Repository\Index\IndexInterface;
use App\DataSource\Validator\UserValidator;
use App\Domain\GetUsersCountRequest;
use RuntimeException;

class UserRepository implements UserRepositoryInterface
{
    private ReaderInterface $userReader;

    private UserValidator $userValidator;

    private UserMapper $userMapper;

    /** @var IndexInterface[] */
    private array $indexes;

    public function __construct(
        ReaderInterface $userReader,
        UserValidator $userValidator,
        UserMapper $userMapper,
        array $indexes
    ) {
        $this->userReader = $userReader;
        $this->userValidator = $userValidator;
        $this->userMapper = $userMapper;
        $this->indexes = $indexes;

        $this->buildIndexes();
    }

    public function getUsersCount(GetUsersCountRequest $request): int
    {
        foreach ($this->indexes as $index) {
            if ($index->isAcceptable($request)) {
                return $index->getUsersCount($request);
            }
        }

        throw new RuntimeException('Index not found.');
    }

    private function buildIndexes(): void
    {
        foreach ($this->userReader->read() as $row) {
            $this->userValidator->validate($row);

            $user = $this->userMapper->map($row);

            foreach ($this->indexes as $index) {
                $index->addUser($user);
            }
        }
    }
}