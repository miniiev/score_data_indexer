<?php

declare(strict_types=1);

namespace App\DataSource\Repository\Index;

use App\Domain\GetUsersCountRequest;
use App\Domain\User;

interface IndexInterface
{
    public function addUser(User $user): void;

    public function isAcceptable(GetUsersCountRequest $request): bool;

    public function getUsersCount(GetUsersCountRequest $request): int;
}