<?php

declare(strict_types=1);

namespace App\DataSource\Repository\Index;

use App\DataSource\Repository\Index\Storage\BinarySearchTree;
use App\Domain\GetUsersCountRequest;
use App\Domain\User;
use InvalidArgumentException;

class ScoreRangeIndex implements IndexInterface
{
    private BinarySearchTree $storage;

    public function __construct(BinarySearchTree $storage)
    {
        $this->storage = $storage;
    }

    public function addUser(User $user): void
    {
        $this->storage->add($user->getScore()->getValue(), 1);
    }

    public function isAcceptable(GetUsersCountRequest $request): bool
    {
        return $request->hasScoreRange();
    }

    public function getUsersCount(GetUsersCountRequest $request): int
    {
        if (!$this->isAcceptable($request)) {
            throw new InvalidArgumentException('GetUsersCountRequest is not acceptable');
        }

        $scoreRange = $request->getScoreRange();

        return $this->storage->getSumByRange($scoreRange->getStart()->getValue(), $scoreRange->getEnd()->getValue());
    }
}