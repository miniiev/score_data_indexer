<?php

declare(strict_types=1);

namespace App\DataSource\Repository\Index;

use App\DataSource\Repository\Index\Storage\HashMap;
use App\Domain\GetUsersCountRequest;
use App\Domain\User;
use InvalidArgumentException;

class ConditionIndex implements IndexInterface
{
    private HashMap $storage;

    public function __construct(HashMap $storage)
    {
        $this->storage = $storage;
    }

    public function addUser(User $user): void
    {
        $storageKey = $this->buildUserStorageKey($user);

        $existedUsersCount = $this->storage->get($storageKey) ?? 0;

        $this->storage->set($storageKey, $existedUsersCount + 1);
    }

    public function isAcceptable(GetUsersCountRequest $request): bool
    {
        return $request->hasRegion()
            && $request->hasGender()
            && $request->hasHasLegalAge()
            && $request->hasHasPositiveScore();
    }

    public function getUsersCount(GetUsersCountRequest $request): int
    {
        if (!$this->isAcceptable($request)) {
            throw new InvalidArgumentException('GetUsersCountRequest is not acceptable');
        }

        return $this->storage->get($this->buildRequestStorageKey($request)) ?? 0;
    }

    private function buildUserStorageKey(User $user): string
    {
        return $user->getRegion() . $user->getGender() . $user->getAge()->isLegal() . $user->getScore()->isPositive();
    }

    private function buildRequestStorageKey(GetUsersCountRequest $request): string
    {
        return $request->getRegion() . $request->getGender() . $request->getHasLegalAge() . $request->getHasPositiveScore();
    }
}