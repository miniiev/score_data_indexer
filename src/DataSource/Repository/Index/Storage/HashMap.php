<?php

declare(strict_types=1);

namespace App\DataSource\Repository\Index\Storage;

class HashMap
{
    private array $storage = [];

    public function set(string $key, int $value)
    {
        $this->storage[$key] = $value;
    }

    public function get(string $key): ?int
    {
        return $this->storage[$key] ?? null;
    }
}