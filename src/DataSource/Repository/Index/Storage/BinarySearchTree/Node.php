<?php

declare(strict_types=1);

namespace App\DataSource\Repository\Index\Storage\BinarySearchTree;

use RuntimeException;

class Node
{
    private int $key;

    private int $value;

    private ?Node $leftChild = null;

    private ?Node $rightChild = null;

    public function __construct(int $key, int $value)
    {
        $this->key = $key;
        $this->value = $value;
    }

    public function getKey(): int
    {
        return $this->key;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    public function equals(Node $node): bool
    {
        return $this->getKey() === $node->getKey();
    }

    public function greater(Node $node): bool
    {
        return $this->getKey() > $node->getKey();
    }

    public function less(Node $node): bool
    {
        return $this->getKey() < $node->getKey();
    }

    public function addValue(Node $node): self
    {
        $this->value += $node->getValue();

        return $this;
    }

    public function setLeftChild(Node $node): self
    {
        $this->leftChild = $node;

        return $this;
    }

    public function hasLeftChild(): bool
    {
        return $this->leftChild !== null;
    }

    public function getLeftChild(): Node
    {
        if ($this->leftChild === null) {
            throw new RuntimeException('Left child not set.');
        }

        return $this->leftChild;
    }

    public function setRightChild(Node $node): self
    {
        $this->rightChild = $node;

        return $this;
    }

    public function hasRightChild(): bool
    {
        return $this->rightChild !== null;
    }

    public function getRightChild(): Node
    {
        if ($this->rightChild === null) {
            throw new RuntimeException('Right child not set.');
        }

        return $this->rightChild;
    }
}