<?php

declare(strict_types=1);

namespace App\DataSource\Repository\Index\Storage;

use App\DataSource\Repository\Index\Storage\BinarySearchTree\Node;
use InvalidArgumentException;

class BinarySearchTree
{
    private ?Node $root = null;

    public function add(int $key, int $value)
    {
        $node = new Node($key, $value);

        if ($this->root === null) {
            $this->root = $node;
            return;
        }

        $this->addToTree($this->root, $node);
    }

    public function getSumByRange(int $fromKey, int $toKey): int
    {
        if ($fromKey > $toKey) {
            throw new InvalidArgumentException('From key is higher than to key.');
        }

        if ($this->root === null) {
            return 0;
        }

        return $this->getSumByRangeInTree($this->root, $fromKey, $toKey);
    }

    private function getSumByRangeInTree(Node $tree, int $fromKey, int $toKey): int
    {
        $sum = 0;

        if ($tree->hasLeftChild() && $fromKey <= $tree->getKey() ) {
            $sum += $this->getSumByRangeInTree($tree->getLeftChild(), $fromKey, $toKey);
        }

        if ($tree->hasRightChild() && $toKey >= $tree->getKey()) {
            $sum += $this->getSumByRangeInTree($tree->getRightChild(), $fromKey, $toKey);
        }

        if ($tree->getKey() >= $fromKey && $tree->getKey() <= $toKey) {
            $sum += $tree->getValue();
        }

        return $sum;
    }

    private function addToTree(Node $tree, Node $node): void
    {
        if ($node->equals($tree)) {
            $tree->addValue($node);
            return;
        }

        if ($node->less($tree)) {
            if (!$tree->hasLeftChild()) {
                $tree->setLeftChild($node);
                return;
            }

            $this->addToTree($tree->getLeftChild(), $node);
            return;
        }

        if ($node->greater($tree)) {
            if (!$tree->hasRightChild()) {
                $tree->setRightChild($node);
                return;
            }

            $this->addToTree($tree->getRightChild(), $node);
            return;
        }
    }
}