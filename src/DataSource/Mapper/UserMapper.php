<?php

declare(strict_types=1);

namespace App\DataSource\Mapper;

use App\Domain\User;
use App\Domain\User\Age;
use App\Domain\User\Gender;
use App\Domain\User\Name;
use App\Domain\User\Region;
use App\Domain\User\Score;

class UserMapper
{
    public function map(array $data): User
    {
        return new User(
            new Name((string)$data['Name']),
            new Gender((string)$data['Gender']),
            new Age((int)$data['Age']),
            new Region((string)$data['Region']),
            new Score((int)$data['Score'])
        );
    }
}