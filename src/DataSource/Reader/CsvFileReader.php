<?php

declare(strict_types=1);

namespace App\DataSource\Reader;

use Exception;
use SplFileObject;

class CsvFileReader implements ReaderInterface
{
    private const DELIMITERS = [';', ':'];

    private SplFileObject $resource;

    public function __construct(SplFileObject $resource)
    {
        $this->resource = $resource;
    }

    public function read(): array
    {
        $this->resource->rewind();

        $headings = $this->splitRow($this->resource->fgets());

        $result = [];

        while (!$this->resource->eof() && ($row = $this->resource->fgets()) !== false) {
            if (empty(trim($row))) {
                continue;
            }

            $result[] = $this->prepareResult($this->splitRow($row), $headings);
        }

        return $result;
    }

    private function prepareResult(array $row, array $headings): array
    {
        if (count($row) !== count($headings)) {
            throw new Exception('Invalid csv file.');
        }

        return array_combine($headings, $row);
    }

    private function splitRow(string $row): array
    {
        $splitRegex = '';

        foreach (self::DELIMITERS as $delimiter) {
            $splitRegex .= preg_quote($delimiter);
        }

        $preparedRow = preg_split('/[' . $splitRegex . ']/', $row);

        return array_map('trim', $preparedRow);
    }
}