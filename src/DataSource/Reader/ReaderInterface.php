<?php

declare(strict_types=1);

namespace App\DataSource\Reader;

interface ReaderInterface
{
    public function read(): array;
}