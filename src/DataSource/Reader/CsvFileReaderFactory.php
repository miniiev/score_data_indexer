<?php

declare(strict_types=1);

namespace App\DataSource\Reader;

use SplFileObject;

class CsvFileReaderFactory
{
    public function create(string $fileName): CsvFileReader
    {
        return new CsvFileReader(new SplFileObject($fileName, 'r'));
    }
}