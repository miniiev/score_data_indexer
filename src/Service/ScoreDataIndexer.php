<?php

declare(strict_types=1);

namespace App\Service;

use App\DataSource\Repository\UserRepositoryInterface;
use App\Domain\GetUsersCountRequest;
use App\Domain\User\Gender;
use App\Domain\User\Region;
use App\Domain\User\Score;
use App\Domain\User\Score\Range;

class ScoreDataIndexer implements ScoreDataIndexerInterface
{
    private UserRepositoryInterface $userRepository;

    public function __construct(
        UserRepositoryInterface $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function getCountOfUsersWithinScoreRange(
        int $rangeStart,
        int $rangeEnd
    ): int {
        return $this->userRepository->getUsersCount(
            GetUsersCountRequest::createScoreRange(
                new Range(new Score($rangeStart), new Score($rangeEnd))
            )
        );
    }

    public function getCountOfUsersByCondition(
        string $region,
        string $gender,
        bool $hasLegalAge,
        bool $hasPositiveScore
    ): int {
        return $this->userRepository->getUsersCount(
            GetUsersCountRequest::createCondition(
                new Region($region), new Gender($gender), $hasLegalAge, $hasPositiveScore
            )
        );
    }
}