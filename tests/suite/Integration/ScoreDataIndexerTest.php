<?php

declare(strict_types=1);

namespace App\Test\Integration;

use App\DataSource\Mapper\UserMapper;
use App\DataSource\Reader\CsvFileReaderFactory;
use App\DataSource\Repository\Index\ConditionIndex;
use App\DataSource\Repository\Index\ScoreRangeIndex;
use App\DataSource\Repository\Index\Storage\BinarySearchTree;
use App\DataSource\Repository\Index\Storage\HashMap;
use App\DataSource\Repository\UserRepository;
use App\DataSource\Validator\UserValidator;
use App\Service\ScoreDataIndexer;
use PHPUnit\Framework\TestCase;

class ScoreDataIndexerTest extends TestCase
{
    private ScoreDataIndexer $sut;

    public function setUp(): void
    {
        $reader = (new CsvFileReaderFactory())->create(dirname(dirname(dirname(__FILE__))).'/users.csv');

        $repository = new UserRepository(
            $reader,
            new UserValidator(),
            new UserMapper(),
            [
                new ScoreRangeIndex(new BinarySearchTree()),
                new ConditionIndex(new HashMap())
            ]
        );

        $this->sut = new ScoreDataIndexer($repository);
    }

    /**
     * @dataProvider scoreRangeDataProvider
     */
    public function testGetCountOfUsersWithinScoreRange(int $rangeStart, int $rangeEnd, int $expectedCountOfUsers): void
    {
        $countOfUsers = $this->sut->getCountOfUsersWithinScoreRange($rangeStart, $rangeEnd);

        $this->assertEquals($expectedCountOfUsers, $countOfUsers);
    }

    /**
     * @dataProvider conditionDataProvider
     */
    public function testGetCountOfUsersByCondition(
        string $region,
        string $gender,
        bool $hasLegalAge,
        bool $hasPositiveScore,
        int $expectedCountOfUsers
    ): void {
        $countOfUsers = $this->sut->getCountOfUsersByCondition($region, $gender, $hasLegalAge, $hasPositiveScore);

        $this->assertEquals($expectedCountOfUsers, $countOfUsers);
    }

    public function scoreRangeDataProvider(): array
    {
        return [
            [
                'range_start' => 20,
                'range_end' => 50,
                'expected_count_of_users' => 3,
            ],
            [
                'range_start' => -40,
                'range_end' => 0,
                'expected_count_of_users' => 1,
            ],
            [
                'range_start' => 0,
                'range_end' => 80,
                'expected_count_of_users' => 4,
            ]
        ];
    }

    public function conditionDataProvider(): array
    {
        return [
            [
                'region' => 'CA',
                'gender' => 'w',
                'has_legal_age' => false,
                'has_positive_score' => false,
                'expected_count_of_users' => 1,
            ],
            [
                'region' => 'CA',
                'gender' => 'w',
                'has_legal_age' => false,
                'has_positive_score' => true,
                'expected_count_of_users' => 0,
            ],
            [
                'region' => 'CA',
                'gender' => 'w',
                'has_legal_age' => true,
                'has_positive_score' => true,
                'expected_count_of_users' => 1,
            ]
        ];
    }
}