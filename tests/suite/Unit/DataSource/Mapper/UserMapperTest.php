<?php

declare(strict_types=1);

namespace App\Test\Unit\DataSource\Mapper;

use App\DataSource\Mapper\UserMapper;
use App\Domain\User;
use App\Domain\User\Age;
use App\Domain\User\Gender;
use App\Domain\User\Name;
use App\Domain\User\Region;
use App\Domain\User\Score;
use PHPUnit\Framework\TestCase;

class UserMapperTest extends TestCase
{
    public function testMap(): void
    {
        $name = 'Jaqueline';
        $gender = 'w';
        $age = '21';
        $region = 'CA';
        $score = '20';

        $expectedUser = new User(
            new Name($name),
            new Gender($gender),
            new Age((int)$age),
            new Region($region),
            new Score((int)$score)
        );

        $sut = new UserMapper();
        $user = $sut->map([
            'Name' => $name,
            'Gender' => $gender,
            'Age' => $age,
            'Region' => $region,
            'Score' => $score,
        ]);

        $this->assertEquals($expectedUser, $user);
    }
}